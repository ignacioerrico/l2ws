﻿namespace TestDll
{
    using System;

    public class SimpleCalculator
    {
        private double memory;

        protected bool isInErrorState;

        public double Memory
        {
            get
            {
                if (isInErrorState)
                {
                    throw new Exception("In error state.");
                }
                return memory;
            }
            set
            {
                memory = value;
            }
        }

        public bool IsInErrorState
        {
            get
            {
                return isInErrorState;
            }
        }

        public SimpleCalculator()
        {
            Memory = 0;
            isInErrorState = false;
        }

        public void Clear()
        {
            Memory = 0;
            isInErrorState = false;
        }

        public void Add(double x)
        {
            Memory += x;
        }

        public void Add(double x, double y)
        {
            Memory = x + y;
        }
        
        public void Substract(double x)
        {
            Memory -= x;
        }

        public void Substract(double x, double y)
        {
            Memory = x - y;
        }
    }

    public class Calculator : SimpleCalculator
    {
        public void Multiply(double x, double y)
        {
            Memory = x * y;
        }
        
        public void Divide(double x, double y)
        {
            if (Math.Abs(y) > 1e-3)
            {
                Memory = x / y;
            }
            else
            {
                isInErrorState = true;
            }
        }
    }
}
