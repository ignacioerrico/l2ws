﻿namespace Reflection
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Reflection;

    public class Program
    {
        static void Main(string[] args)
        {
            var assemblies = new List<string>();

            // Parameter validation
            foreach (var arg in args)
            {
                if (File.Exists(arg))
                {
                    assemblies.Add(arg);
                }
                else if (Directory.Exists(arg))
                {
                    assemblies.AddRange(Directory.GetFiles(arg, "*.dll", SearchOption.AllDirectories));
                }
                else
                {
                    Console.WriteLine(arg + " is neither a file nor a directory.\n");
                    goto Finish;
                }
            }

            // If no file (or directory) was specified on the command line, display help
            if (assemblies.Count == 0)
            {
                var exeFilename = AppDomain.CurrentDomain.FriendlyName;
                Console.WriteLine("Usage: " + exeFilename + " [files]\n"
                    + "Where [files] is a list of assembly files and/or directories.\n");

                goto Finish;
            }

            Console.WriteLine(assemblies.Count + " file(s) found.\n");

            // Reflect on assemblies
            foreach (var assemblyName in assemblies)
            {
                var assembly = Assembly.LoadFile(Path.GetFullPath(assemblyName));
                ReflectOnAssembly(assembly);
            }

        Finish:

#if DEBUG
            Console.Write("Press <ENTER> to exit.");
            Console.ReadKey();
#endif
        }

        private static void ReflectOnAssembly(Assembly assembly)
        {
            Console.WriteLine("Reflecting on: {0}", Path.GetFileName(assembly.CodeBase));
            Console.WriteLine("({0})\n", assembly.FullName);

            var types = assembly.GetTypes();

            var namespaces = (from t in types select t.Namespace).Distinct();

            foreach (var ns in namespaces)
            {
                Console.WriteLine("Namespace: {0}", ns);

                var nsStr = ns;
                foreach (var type in types.Where(type => type.IsClass && type.Namespace == nsStr))
                {
                    Console.WriteLine("  Class: {0}", type.Name);

                    var properties =
                        type.GetProperties(BindingFlags.Public | BindingFlags.Instance | BindingFlags.DeclaredOnly);
                    foreach (var propertyInfo in properties)
                    {
                        Console.WriteLine("    Property: [{0}] {1}", propertyInfo.PropertyType.Name, propertyInfo.Name);
                    }

                    var methods =
                        type.GetMethods(BindingFlags.Public | BindingFlags.Instance | BindingFlags.DeclaredOnly)
                            .Where(m => !m.IsSpecialName);
                    foreach (var methodInfo in methods)
                    {
                        var parameters = from p in methodInfo.GetParameters() select p.ParameterType.Name + " " + p.Name;
                        var parametersText = string.Join(", ", parameters);

                        Console.WriteLine(
                            "    Method: [{0}] {1} ({2})",
                            methodInfo.ReturnType.Name,
                            methodInfo.Name,
                            parametersText);
                    }
                }
            }

            Console.WriteLine();
        }
    }
}
