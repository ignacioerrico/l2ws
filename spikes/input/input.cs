namespace MusicalInstruments
{
  public class Guitar
  {
    public string Brand { get; set; }

    public void Play()
    {
    }

    public void Tune()
    {
    }
  }

  public class Drums
  {
    public double Price { get; set; }

    public void Paradiddle()
    {
    }
  }
}
