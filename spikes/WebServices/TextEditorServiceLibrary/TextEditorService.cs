﻿namespace TextEditorServiceLibrary
{
    using System;

    public class TextEditorService : IEditText
    {
        public TextEditorService()
        {
            Console.WriteLine("[{0}] The Text Editor Service has been invoked.", DateTime.Now);
        }

        public string ToUpper(string text)
        {
            return text.ToUpper();
        }

        public string ToLower(string text)
        {
            return text.ToLower();
        }
    }
}
