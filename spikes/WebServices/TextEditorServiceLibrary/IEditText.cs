﻿namespace TextEditorServiceLibrary
{
    using System.ServiceModel;

    [ServiceContract]
    public interface IEditText
    {
        [OperationContract]
        string ToUpper(string text);

        [OperationContract]
        string ToLower(string text);
    }
}
