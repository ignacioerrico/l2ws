﻿namespace TextEditorServiceHost
{
    using System;
    using System.ServiceModel;

    using TextEditorServiceLibrary;

    public class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("This is the service host listening on port 8082.\n");

            using (var serviceHost = new ServiceHost(typeof(TextEditorService)))
            {
                serviceHost.Open();

                DisplayHostInfo(serviceHost);

                Console.WriteLine("The service has been started.");
                Console.WriteLine("Press <ENTER> to terminate service.");
                Console.ReadLine();
            }
        }

        static void DisplayHostInfo(ServiceHostBase serviceHost)
        {
            Console.WriteLine("Host information [BEGIN]\n");

            foreach (var se in serviceHost.Description.Endpoints)
            {
                Console.WriteLine("Address: {0}", se.Address);
                Console.WriteLine("Binding: {0}", se.Binding.Name);
                Console.WriteLine("Contract: {0}", se.Contract.Name);
                Console.WriteLine();
            }

            Console.WriteLine("Host information [END]\n");
        }
    }
}
