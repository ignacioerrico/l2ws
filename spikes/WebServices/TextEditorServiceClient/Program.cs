﻿namespace TextEditorServiceClient
{
    using System;

    using TextEditorServiceClient.ServiceReference;

    public class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("This is the service client.  Make sure the host is running.\n");
            
            using (var client = new EditTextClient())
            {
                var command = char.MinValue;

                while (command != 'Q')
                {
                    Console.Write("Press U to convert to uppercase, D for lowercase, Q to quit: ");
                    command = Console.ReadKey().KeyChar;
                    command = char.ToUpper(command);
                    Console.WriteLine();

                    string text = string.Empty;

                    if (command == 'U' || command == 'D')
                    {
                        Console.Write("Input some text: ");
                        text = Console.ReadLine();
                    }

                    if (command == 'U')
                    {
                        var output = client.ToUpper(text);
                        Console.WriteLine("Output: {0}", output);
                    }
                    else if (command == 'D')
                    {
                        var output = client.ToLower(text);
                        Console.WriteLine("Output: {0}", output);
                    }
                    else if (command == 'Q')
                    {
                        break;
                    }
                    else
                    {
                        Console.WriteLine("Error: invalid command!");
                    }

                    Console.WriteLine();
                }
            }

            Console.Write("\nPress <ENTER> to exit this client.");
            Console.ReadLine();
        }
    }
}
