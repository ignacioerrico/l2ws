﻿namespace CSParser
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.IO;

    using DDW;

    public class Program
    {
        static void Main(string[] args)
        {
            var files = new List<string>();

            // Parameter validation
            foreach (var arg in args)
            {
                if (File.Exists(arg))
                {
                    files.Add(arg);
                }
                else if (Directory.Exists(arg))
                {
                    files.AddRange(Directory.GetFiles(arg, "*.cs", SearchOption.AllDirectories));
                }
                else
                {
                    Console.WriteLine(arg + " is neither a file nor a directory.\n");
                    goto Finish;
                }
            }

            // If no file (or directory) was specified on the command line, display help
            if (files.Count == 0)
            {
                var exeFilename = AppDomain.CurrentDomain.FriendlyName;
                Console.WriteLine("Usage: " + exeFilename + " [files]\n"
                    + "Where [files] is a list of files and/or directories.\n");

                goto Finish;
            }

            Console.WriteLine(files.Count + " file(s) found.\n");

            var sw = new Stopwatch();
            sw.Reset();

            // Parse files
            foreach (var fileName in files)
            {
                sw.Start();
                ParseFile(fileName);
                sw.Stop();
            }

            Console.WriteLine("\nTotal time: " + Math.Round(sw.Elapsed.TotalSeconds, 2) + " seconds, for " + files.Count + " file(s).\n");

        Finish:

#if DEBUG
            Console.Write("Press <ENTER> to exit.");
            Console.ReadKey();
#endif
        }

        private static void ParseFile(string fileName)
        {
            Console.WriteLine("Parsing: {0}\n", fileName);

            var fs = new FileStream(fileName, FileMode.Open, FileAccess.Read);
            var sr = new StreamReader(fs, true);
            var l = new Lexer(sr);
            var toks = l.Lex();

            var p = new Parser(fileName);
            var cu = p.Parse(toks, l.StringLiterals);

            if (p.Errors.Count != 0)
            {
                Console.WriteLine("The following errors have been found:\n");
                PrintErrors(p.Errors);
            }
            else
            {
                PrintFileContents(cu);
            }
        }

        private static void PrintFileContents(CompilationUnitNode cu)
        {
            var namespaces = cu.Namespaces;
            if (namespaces.Count == 0)
            {
                Console.WriteLine("The file does not define any namespaces.");
            }
            else
            {
                foreach (var namespaceNode in namespaces)
                {
                    Console.WriteLine("  Namespace: {0}", namespaceNode.Name.GenericIdentifier);

                    var classes = namespaceNode.Classes;
                    if (classes.Count == 0)
                    {
                        Console.WriteLine("    No classes in this namespace.");
                    }
                    else
                    {
                        foreach (var classNode in classes)
                        {
                            PrintClassInfo(classNode);
                        }
                    }
                }
            }
        }

        private static void PrintClassInfo(ClassNode classNode)
        {
            Console.WriteLine("    Class: {0}", classNode.Name.Identifier);

            var properties = classNode.Properties;
            foreach (var propertyNode in properties)
            {
                Console.WriteLine(
                    "      Property: {0} ({1})",
                    propertyNode.Names[0].GenericIdentifier,
                    ((PredefinedTypeNode)propertyNode.Type).Identifier.GenericIdentifier);
            }

            var methods = classNode.Methods;
            foreach (var methodNode in methods)
            {
                Console.WriteLine(
                    "      Method: {0} ({1})",
                    methodNode.Names[0].GenericIdentifier,
                    ((PredefinedTypeNode)methodNode.Type).Identifier.GenericIdentifier);
            }
        }

        private static void PrintErrors(IEnumerable<Parser.Error> errors)
        {
            foreach (var error in errors)
            {
                if (error.Token.ID == TokenID.Eof && error.Line == -1)
                {
                    Console.WriteLine(error.Message + "File: " + error.FileName + "\n");
                }
                else
                {
                    Console.WriteLine("  " + error.Message + " in token " + error.Token.ID
                        + "\n  line: " + error.Line
                        + ", column: " + error.Column
                        + ", file: " + error.FileName + "\n");
                }
            }
        }

    }
}
