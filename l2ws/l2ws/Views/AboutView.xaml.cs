﻿namespace l2ws.Views
{
    using l2ws.ViewModels;

    using System.Windows;

    /// <summary>
    /// Interaction logic for AboutView.xaml
    /// </summary>
    public partial class AboutView : Window
    {
        public AboutView()
        {
            InitializeComponent();
            DataContext = new AboutViewModel(this);
        }
    }
}
