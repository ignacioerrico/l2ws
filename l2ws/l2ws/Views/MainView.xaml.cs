﻿namespace l2ws.Views
{
    using System.Windows;

    using l2ws.ViewModels;

    /// <summary>
    /// Interaction logic for MainView.xaml
    /// </summary>
    public partial class MainView : Window
    {
        public MainView()
        {
            InitializeComponent();
            DataContext = new MainViewModel(this);
        }
    }
}
