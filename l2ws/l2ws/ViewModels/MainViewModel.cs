﻿namespace l2ws.ViewModels
{
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.IO;
    using System.Reflection;
    using System.Runtime.CompilerServices;
    using System.Windows;

    using l2ws.Annotations;
    using l2ws.Models.CodeInformationExtractors;
    using l2ws.ViewModels.TreeView;
    using l2ws.Views;

    using Microsoft.Practices.Prism.Commands;
    using Microsoft.Win32;

    public class MainViewModel : INotifyPropertyChanged
    {
        private readonly MainView mainView;

        private string fileName;

        private List<ICheckableNode> solution;

        public string FileName
        {
            get
            {
                return fileName;
            }
            set
            {
                fileName = value;
                OnPropertyChanged("IsFileNameSet");
                
            }
        }

        public bool IsFileNameSet
        {
            get
            {
                var isFileNameSet = !string.IsNullOrEmpty(fileName);
                mainView.StatusLabel.Content = isFileNameSet ? fileName : "No file loaded.";
                return isFileNameSet;
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public DelegateCommand FileLoadDllCommand { get; set; }

        public DelegateCommand FileCloseCommand { get; set; }

        public DelegateCommand FileSettingsCommand { get; set; }
        
        public DelegateCommand FileExitCommand { get; set; }
        
        public DelegateCommand HelpAboutCommand { get; set; }

        public List<ICheckableNode> Solution
        {
            get
            {
                return solution;
            }
            set
            {
                solution = value;
                OnPropertyChanged();
            }
        }

        public MainViewModel(MainView mainView)
        {
            this.mainView = mainView;

            FileLoadDllCommand = new DelegateCommand(FileLoadDllCommandOnExecute);
            FileCloseCommand = new DelegateCommand(FileCloseCommandOnExecute);
            FileSettingsCommand = new DelegateCommand(FileSettingsCommandOnExecute);
            FileExitCommand = new DelegateCommand(FileExitCommandOnExecute);
            HelpAboutCommand = new DelegateCommand(HelpAboutCommandOnExecute);
        }

        private void FileLoadDllCommandOnExecute()
        {
            var openFileDialog = new OpenFileDialog { DefaultExt = ".dll", Filter = "DLL Files (*.dll)|*.dll" };

            var result = openFileDialog.ShowDialog();
            if (result == true)
            {
                FileName = openFileDialog.FileName;

                var reflector = new Reflector();
                var assembly = Assembly.LoadFile(Path.GetFullPath(FileName));

                var treeViewItems = reflector.CreateTreeViewItemsFrom(assembly);
                Solution = new List<ICheckableNode> { treeViewItems };

                mainView.TreeView.Focus();
            }
        }

        private void FileCloseCommandOnExecute()
        {
            FileName = string.Empty;
            Solution = new List<ICheckableNode>();
        }

        private void FileSettingsCommandOnExecute()
        {
            MessageBox.Show("To be implemented", "TBI", MessageBoxButton.OK);
        }

        private void FileExitCommandOnExecute()
        {
            mainView.Close();
        }

        private void HelpAboutCommandOnExecute()
        {
            var aboutView = new AboutView();
            aboutView.ShowDialog();
        }

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            var handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
