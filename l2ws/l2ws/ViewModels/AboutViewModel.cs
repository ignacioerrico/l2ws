﻿namespace l2ws.ViewModels
{
    using l2ws.Views;

    using Microsoft.Practices.Prism.Commands;

    public class AboutViewModel
    {
        private readonly AboutView aboutView;
        
        public DelegateCommand CloseCommand { get; set; }

        public AboutViewModel(AboutView aboutView)
        {
            this.aboutView = aboutView;

            CloseCommand = new DelegateCommand(CloseCommandOnExecute);
        }

        private void CloseCommandOnExecute()
        {
            aboutView.Close();
        }
    }
}
