﻿namespace l2ws.ViewModels.TreeView
{
    using System.Collections.Generic;
    using System.Windows.Controls;

    public class Solution : CheckableNode
    {
        public Solution(string name, TextBlock formattedCode, List<CheckableNode> namespaces)
            : base(namespaces)
        {
            NodeName = name;
            UnderlyingCode = name;
            FormattedCode = formattedCode;
            IsChecked = false;
            Namespaces = namespaces;
        }

        public List<CheckableNode> Namespaces { get; private set; }
    }
}
