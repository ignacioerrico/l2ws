﻿namespace l2ws.ViewModels.TreeView
{
    using System.Collections.Generic;
    using System.Windows.Controls;

    public class Class : CheckableNode
    {
        public Class(string name, string underlyingCode, TextBlock formattedCode, List<CheckableNode> methods)
            : base(methods)
        {
            NodeName = name;
            UnderlyingCode = underlyingCode;
            FormattedCode = formattedCode;
            IsChecked = false;
            Methods = methods;
        }

        public List<CheckableNode> Methods { get; private set; }
    }
}