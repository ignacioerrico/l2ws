﻿namespace l2ws.ViewModels.TreeView
{
    using System.Collections.Generic;
    using System.Windows.Controls;

    public class Method : CheckableNode
    {
        public Method(string name, string underlyingCode, TextBlock formattedCode)
            : base(new List<CheckableNode>())
        {
            NodeName = name;
            UnderlyingCode = underlyingCode;
            FormattedCode = formattedCode;
            IsChecked = false;
        }
    }
}