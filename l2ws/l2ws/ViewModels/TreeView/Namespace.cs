﻿namespace l2ws.ViewModels.TreeView
{
    using System.Collections.Generic;
    using System.Windows.Controls;

    public class Namespace : CheckableNode
    {
        public Namespace(string name, string underlyingCode, TextBlock formattedCode, List<CheckableNode> classes)
            : base(classes)
        {
            NodeName = name;
            UnderlyingCode = underlyingCode;
            FormattedCode = formattedCode;
            IsChecked = false;
            Classes = classes;
        }

        public List<CheckableNode> Classes { get; private set; }
    }
}