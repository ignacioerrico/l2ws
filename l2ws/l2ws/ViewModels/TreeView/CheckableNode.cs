﻿namespace l2ws.ViewModels.TreeView
{
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;
    using System.Windows.Controls;

    using l2ws.Annotations;

    public class CheckableNode : INotifyPropertyChanged, ICheckableNode
    {
        private bool? isChecked = false;

        private CheckableNode parent;

        public CheckableNode(List<CheckableNode> children)
        {
            Children = children;
        }

        public void Initialize()
        {
            foreach (var child in Children)
            {
                child.parent = this;
                child.Initialize();
            }
        }

        public string NodeName { get; protected set; }

        public List<CheckableNode> Children { get; private set; }

        public string UnderlyingCode { get; protected set; }

        public TextBlock FormattedCode { get; protected set; }

        public bool IsInitiallySelected { get; set; }

        public bool? IsChecked
        {
            get { return isChecked; }
            set { SetIsChecked(value, true, true); }
        }

        private void SetIsChecked(bool? value, bool updateChildren, bool updateParent)
        {
            if (value == isChecked)
                return;

            isChecked = value;

            if (updateChildren && isChecked.HasValue)
                Children.ForEach(c => c.SetIsChecked(isChecked, true, false));

            if (updateParent && parent != null)
                parent.VerifyCheckState();

            OnPropertyChanged("IsChecked");
        }

        private void VerifyCheckState()
        {
            bool? state = null;
            for (int i = 0; i < Children.Count; ++i)
            {
                bool? current = Children[i].IsChecked;
                if (i == 0)
                {
                    state = current;
                }
                else if (state != current)
                {
                    state = null;
                    break;
                }
            }
            SetIsChecked(state, false, true);
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            var handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}