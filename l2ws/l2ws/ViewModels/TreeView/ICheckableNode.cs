namespace l2ws.ViewModels.TreeView
{
    using System.Windows.Controls;

    public interface ICheckableNode
    {
        string NodeName { get; }

        string UnderlyingCode { get; }

        TextBlock FormattedCode { get; }

        bool IsInitiallySelected { get; set; }

        bool? IsChecked { get; set; }
    }
}