﻿namespace l2ws.Models.CodeInformationExtractors
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Windows.Controls;
    using System.Windows.Documents;

    using l2ws.ViewModels.TreeView;

    public class Reflector : IExtractCodeInformationFrom<Assembly>
    {
        /// <summary>
        /// http://stackoverflow.com/questions/1362884/is-there-a-way-to-get-a-types-alias-through-reflection
        /// </summary>
        private static readonly Dictionary<string, string> Aliases = new Dictionary<string, string> {
            { typeof(byte).ToString(), "byte" },
            { typeof(sbyte).ToString(), "sbyte" },
            { typeof(short).ToString(), "short" },
            { typeof(ushort).ToString(), "ushort" },
            { typeof(int).ToString(), "int" },
            { typeof(uint).ToString(), "uint" },
            { typeof(long).ToString(), "long" },
            { typeof(ulong).ToString(), "ulong" },
            { typeof(float).ToString(), "float" },
            { typeof(double).ToString(), "double" },
            { typeof(decimal).ToString(), "decimal" },
            { typeof(object).ToString(), "object" },
            { typeof(bool).ToString(), "bool" },
            { typeof(char).ToString(), "char" },
            { typeof(string).ToString(), "string" },
            { typeof(void).ToString(), "void" }
        };

        public Solution CreateTreeViewItemsFrom(Assembly resource)
        {
            var types = resource.GetTypes();
            var namespaces = (from t in types select t.Namespace).Distinct();

            var namespaceNodes = new List<CheckableNode>();

            foreach (var ns in namespaces)
            {
                var classNodes = new List<CheckableNode>();

                var nsStr = ns;
                foreach (var type in types.Where(type => type.IsClass && type.Namespace == nsStr))
                {
                    var methodNodes = new List<CheckableNode>();

                    var methods =
                        type.GetMethods(BindingFlags.Public | BindingFlags.Instance | BindingFlags.DeclaredOnly)
                            .Where(m => !m.IsSpecialName);
                    foreach (var methodInfo in methods)
                    {
                        var parameters = from p in methodInfo.GetParameters() select GetTypeAlias(p.ParameterType.FullName) + " " + p.Name;
                        var parametersText = string.Join(", ", parameters);

                        var methodName = methodInfo.Name;
                        var methodCode = string.Format(
                            "public {0} {1} ({2})",
                            GetTypeAlias(methodInfo.ReturnType.FullName),
                            methodInfo.Name,
                            parametersText);
                        var methodFormatted = new TextBlock();
                        methodFormatted.Inlines.Add(new Run("public "));
                        methodFormatted.Inlines.Add(new Bold(new Run(GetTypeAlias(methodInfo.ReturnType.FullName))));
                        methodFormatted.Inlines.Add(new Run(" "));
                        methodFormatted.Inlines.Add(new Bold(new Run(methodInfo.Name)));
                        methodFormatted.Inlines.Add(new Run("("));
                        methodFormatted.Inlines.Add(new Run(parametersText));
                        methodFormatted.Inlines.Add(new Run(")"));
                        var methodItem = new Method(methodName, methodCode, methodFormatted);
                        methodNodes.Add(methodItem);
                    }

                    var className = type.Name;
                    var classCode = string.Format("public class {0}", className);
                    var classFormatted = new TextBlock();
                    classFormatted.Inlines.Add(new Run("public class "));
                    classFormatted.Inlines.Add(new Bold(new Run(className)));
                    var classItem = new Class(className, classCode, classFormatted, methodNodes);
                    classNodes.Add(classItem);
                }

                var namespaceCode = string.Format("namespace {0}", nsStr);
                var namespaceFormatted = new TextBlock();
                namespaceFormatted.Inlines.Add(new Run("namespace "));
                namespaceFormatted.Inlines.Add(new Bold(new Run(nsStr)));
                var namespaceItem = new Namespace(nsStr, namespaceCode, namespaceFormatted, classNodes);
                namespaceNodes.Add(namespaceItem);
            }

            var assemblyName = resource.ManifestModule.Name;
            var assemblyFormatted = new TextBlock();
            assemblyFormatted.Inlines.Add(new Run("Assembly "));
            assemblyFormatted.Inlines.Add(new Bold(new Run(assemblyName)));
            var solutionItem = new Solution(assemblyName, assemblyFormatted, namespaceNodes)
            {
                IsInitiallySelected = true
            };
            solutionItem.Initialize();

            return solutionItem;
        }

        private string GetTypeAlias(string typeName)
        {
            if (typeName == null)
            {
                return "null";
            }

            return Aliases.ContainsKey(typeName) ? Aliases[typeName] : typeName;
        }
    }
}
