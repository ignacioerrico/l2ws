﻿namespace l2ws.Models.CodeInformationExtractors
{
    using l2ws.ViewModels.TreeView;

    public interface IExtractCodeInformationFrom<in T>
    {
        Solution CreateTreeViewItemsFrom(T resource);
    }
}
